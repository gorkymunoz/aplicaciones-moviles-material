package com.example.aplicacionmaterial

import com.google.gson.annotations.SerializedName

data class WeatherResponse(var weather: List<Wheater>?) {}

data class Wheater(
    @SerializedName("main")
    var name:String?,
    var description:String?,
    var icon:String?) {}
