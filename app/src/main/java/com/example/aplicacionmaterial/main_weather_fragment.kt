package com.example.aplicacionmaterial


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class main_weather_fragment : Fragment() {

    lateinit var cityName:TextView
    lateinit var weatherName: TextView
    lateinit var searchButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_weather_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityName = view.findViewById(R.id.city_name)
        weatherName = view.findViewById(R.id.weather_response)
        searchButton = view.findViewById(R.id.search_button)

        searchButton.setOnClickListener { getWeather() }
    }

    fun getWeather(){

        val weatherService = WeatherService.instance
        var weatherCall = weatherService.getWeather("2.5",cityName.text.toString())

        weatherCall.enqueue(object: Callback<WeatherResponse> {
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
            }
            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                val weatherList = response.body() as WeatherResponse
                val firstWeather = weatherList.weather?.get(0)
                weatherName.text = firstWeather?.name ?: "Error"
            }
        })
    }
}
